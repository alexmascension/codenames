import locale
locale.setlocale(locale.LC_ALL, '') # To sort strings with tildes and other letters

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib

from herramientas import ajuste_ax, cmp_to_key, floritura

# Ajuste de fuente
import matplotlib.font_manager as fm

prop = fm.FontProperties(fname='DK Majolica.otf')

import os

from random import shuffle


dir_palabras = "Palabras/"
archivos_impresos = ["Duo.txt", 'Codenames.txt', 'Undercover.txt', 'Random_words_07_2019.txt']
archivos_no_impresos = []
# Busca todos los archivos en dir_palabras que no estén en archivos_impresos
for ROOT, DIRS, FILES in os.walk(dir_palabras):
    for file in FILES:
        if file not in archivos_impresos:
            archivos_no_impresos.append(file)


# Cargamos la lista de palabras impresas y no impresas
palabras_impresas, palabras_no_impresas = [], []


for file in archivos_impresos:
    print(dir_palabras + file)

    lista_file = [i.strip(' ').strip('\t').upper() for i in open(dir_palabras + file, 'r', encoding='ISO-8859-1').read().split('\n')]
    palabras_impresas += lista_file

for file in archivos_no_impresos:
    lista_file = [i.strip(' ').strip('\t').upper() for i in open(dir_palabras + file, 'r', encoding='ISO-8859-1').read().split('\n')]
    palabras_no_impresas += lista_file


# Eliminamos espacios
palabras_impresas = [i for i in palabras_impresas if len(i) > 0]
palabras_no_impresas = [i for i in palabras_no_impresas if len(i) > 0]


# Eliminamos palabras repetidas en ambas listas
palabras_impresas = sorted(list(dict.fromkeys(palabras_impresas)), key=cmp_to_key(locale.strcoll))
palabras_no_impresas = sorted(list(dict.fromkeys(palabras_no_impresas)), key=cmp_to_key(locale.strcoll))


# Eliminamos palabras repetidas en palabras impresas para palabras no impresas
palabras_no_impresas = [i.replace('Ã±', 'Ñ') for i in palabras_no_impresas if i not in palabras_impresas]


# Cambiamos letras raras por sus correctas
palabras_no_impresas = [i.replace('Ã±', 'Ñ').replace('Ã¡', 'Á').replace('Ãº', 'Ú').replace('Ã\xad', 'Í').replace('Ã³', 'Ó').replace('Ã©', 'È') for i in palabras_no_impresas]


# Randomizar orden de las palabras
shuffle(palabras_no_impresas)

print(palabras_no_impresas)

# Especificamos directorios para las cartas
dir_cartas = "Cartas/"
if not os.path.exists(dir_cartas): os.mkdir(dir_cartas)


# Aplicamos el diseño de la página
n_filas = 5
n_columnas = 5
ancho_carta = 5.8 #cm
alto_carta = 3.9 #cm


# Ahora vamos a aplicar el diseño de la figura por cada conjunto de palabras
for i in range(0, len(palabras_no_impresas), n_filas*n_columnas):

    if i + n_columnas*n_filas > len(palabras_no_impresas):
        palabras_figura = palabras_no_impresas[i:]
    else:
        palabras_figura = palabras_no_impresas[i:i+n_filas*n_columnas]

    nombre_figura = "Cartas_%s_%s" %(i, i+n_columnas*n_filas)

    print(nombre_figura, palabras_figura)

    fig = plt.figure(figsize=(n_columnas*ancho_carta, n_filas*alto_carta))
    plt.axis('off')
    gs1 = gridspec.GridSpec(n_filas, n_columnas)
    gs1.update(wspace=0.0, hspace=0.0)  # set the spacing between axes.

    for c in range(n_columnas*n_filas):
        ax = plt.subplot(gs1[c])
        ax.text(0, -0.3, palabras_figura[c], horizontalalignment='center',
             verticalalignment = 'center', fontproperties=prop, size=33)

        ax.text(0, 0.3, palabras_figura[c], horizontalalignment='center',
             verticalalignment = 'center', rotation = 180, fontproperties=prop, size=27, color='#a2a2a2')
        ajuste_ax(ax, n_filas, n_columnas, c)

        floritura(ax)



    plt.savefig(dir_cartas + nombre_figura+'.jpg',bbox_inches='tight', pad_inches=0, dpi=180, quality=100, optimize=True)
    plt.clf()








