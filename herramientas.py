import matplotlib.pyplot as plt

def ajuste_ax(ax, n_filas, n_columnas, i):
    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_xticks([])
    ax.set_yticks([])

    gris = '#dbdbdb'

    ax.spines['bottom'].set_color(gris)
    ax.spines['right'].set_color(gris)
    ax.spines['top'].set_color(gris)
    ax.spines['left'].set_color(gris)



def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'

    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj

        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0

        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0

        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0

        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0

        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0

        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0

    return K


def floritura(ax):
    pos_base = 0.6
    pos_lf0, pos_lg, pos_lf, pos_cd = pos_base-0.03, pos_base, pos_base+0.03, pos_base+0.07

    lg = 1.5
    lf = 1
    cd = 1

    color = "#909090"

    # Linea fina
    ax.plot([-0.6, 0.6], [pos_lf0, pos_lf0], linewidth=lf, color=color)
    ax.plot([-0.6, 0.6], [-pos_lf0, -pos_lf0], linewidth=lf, color=color)

    # Linea gorda
    ax.plot([-0.77, 0.77], [pos_lg, pos_lg],  linewidth=lg, color=color)
    ax.plot([-0.77, 0.77], [-pos_lg, -pos_lg],  linewidth=lg, color=color)

    # Linea fina
    ax.plot([-0.7, 0.7], [pos_lf, pos_lf],  linewidth=lf, color=color)
    ax.plot([-0.7, 0.7], [-pos_lf, -pos_lf],  linewidth=lf, color=color)

    # Cuadrados
    ax.plot([-0.5, 0.5], [pos_cd, pos_cd],  linewidth=cd, color=color)
    ax.plot([-0.5, -0.5], [pos_cd, pos_lf], linewidth=cd, color=color)
    ax.plot([0.5, 0.5], [pos_cd, pos_lf],  linewidth=cd, color=color)

    ax.plot([-0.5, 0.5], [-pos_cd, -pos_cd],  linewidth=cd, color=color)
    ax.plot([-0.5, -0.5], [-pos_cd, -pos_lf],  linewidth=cd, color=color)
    ax.plot([0.5, 0.5], [-pos_cd, -pos_lf],  linewidth=cd, color=color)

